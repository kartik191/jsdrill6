const data = require('./data.json');
const drills = require('./drills');


console.log(drills.getPersonWithSpecificJob(data, 'Web Developer'));

console.log(drills.convertSalaiesToNumber(data));

console.log(drills.addCorrectedSalary(data, 10000));

console.log(drills.getSalarySum(data));

console.log(drills.getCountrySalarySum(data));

console.log(drills.getCountryAverageSalary(data));