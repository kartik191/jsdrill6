function getPersonWithSpecificJob(data, job){
    if(typeof(job) !== 'string'){
        job = 'Web Developer';
    }
    return data.filter(person => person.job.includes(job));
}

function convertSalaiesToNumber(data){
    return data.map(person => {
        person.salary = Number(person.salary.replace(/[,$]/, ''));
        return person;
    });
}

function addCorrectedSalary(data, factor){
    if(typeof(factor) !== 'number'){
        factor = 10000;
    }
    return data.map(person => {
        person['corrected_salary'] = person.salary * factor;
        return person;
    });
}

function getSalarySum(data){
    return data.reduce((result, person) => {
        result += person.corrected_salary;
        return result;
    }, 0);
}

function getCountrySalarySum(data){
    return data.reduce((result, person) => {
        let salary = person.corrected_salary;
        let country = person.location;
        result[country] === undefined ? result[country] = salary : result[country] += salary;
        return result;
    }, {});
}

function getCountryAverageSalary(data){
    let averageSalary = data.reduce((result, person) => {
        let salary = person.corrected_salary;
        let country = person.location;
        if(result[country] === undefined){
            result[country] = {'salary': 0, 'count': 0};
        }
        result[country].salary += salary;
        result[country].count += 1;
        return result;
    }, {});
    return Object.entries(averageSalary).reduce((result, array) => {
        let avgSalary = array[1].salary / array[1].count;
        let country = array[0];
        result[country] = avgSalary;
        return result;
    }, {});
}


module.exports = { getPersonWithSpecificJob, convertSalaiesToNumber, addCorrectedSalary, getSalarySum,
    getCountrySalarySum , getCountryAverageSalary };